load stream.mat


Mx(1,1)=(Phi(1,2)-Phi(1,1))/(2*dy);
Mx(1,Ny)=(Phi(1,Ny)-Phi(1,Ny-1))/(2*dy);
Mx(Nx,Ny)=(Phi(Nx,Ny)-Phi(Nx,Ny-1))/(2*dy);
Mx(Nx,1)=(Phi(Nx,2)-Phi(Nx,1))/(2*dy);

My(1,1)=(Phi(2,1)-Phi(1,1))/(2*dx);
My(1,Ny)=(Phi(2,Ny)-Phi(1,Ny))/(2*dx);
My(Nx,Ny)=(Phi(Nx,Ny)-Phi(Nx-1,Ny))/(2*dx);
My(Nx,1)=(Phi(Nx,1)-Phi(Nx-1,1))/(2*dx);

for i=2:Nx-1
    Mx(i,1)=(Phi(i,2)-Phi(i,1))/(2*dy);
    Mx(i,Ny)=(Phi(i,Ny)-Phi(i,Ny-1))/(2*dy);
    My(i,1)=(Phi(i+1,1)-Phi(i-1,1))/(2*dx);
    My(i,Ny)=(Phi(i+1,Ny)-Phi(i-1,Ny))/(2*dx);
    for j=2:Ny-1
        Mx(i,j)=(Phi(i,j+1)-Phi(i,j-1))/(2*dy);
        My(i,j)=(Phi(i+1,j)-Phi(i-1,j))/(2*dx);
    end
end
for j=2:Ny-1
    Mx(1,j)=(Phi(1,j+1)-Phi(1,j-1))/(2*dy);
    Mx(Nx,j)=(Phi(Nx,j+1)-Phi(Nx,j-1))/(2*dy);
    My(1,j)=(Phi(2,j)-Phi(1,j))/(2*dx);
    My(Nx,j)=(Phi(Nx,j)-Phi(Nx-1,j))/(2*dx);
end

u=Mx/(rho_eau*H);%u=mean(mean(u))
v=-My/(rho_eau*H);%v=mean(mean(v))

u_graph=u(5:5:Nx,5:5:Ny);
v_graph=v(5:5:Nx,5:5:Ny);

x=[1:Nx];
y=[1:Ny];

figure(1)
pcolor((Phi./mask.*mask)')
hold on
quiver(x(5:5:Nx),y(5:5:Ny),u_graph',v_graph','w','linewidth',2)
hold off
