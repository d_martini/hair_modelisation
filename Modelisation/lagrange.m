%Lagrange
clear xL yL uL vL

Nt=150;
dt=10800;
for ip=1:1
    xL(1,ip)=(130+ip*5)*dx;%En m
    yL(1,ip)=(50+ip*8)*dy;%en m
end
%    uL(1)=u(xL(1)/dx,yL(1)/dy);
%    vL(1)=v(xL(1)/dx,yL(1)/dy);
for ip=1:1
uL(1,ip)=u(130+ip*5,50+8*ip);
vL(1,ip)=v(130+ip*5,50+8*ip);
end

for ip=1:1
for l=2:Nt-1
    xL(l,ip)=xL(l-1,ip)+dt*uL(l-1,ip);
    yL(l,ip)=yL(l-1,ip)+dt*vL(l-1,ip);
    uL(l,ip)=interp2(u',xL(l,ip)/dx,yL(l,ip)/dy);%Interpolation entre deux points de la grille
    vL(l,ip)=interp2(v',xL(l,ip)/dx,yL(l,ip)/dy);
end
end

figure
pcolor((Phi./mask.*mask)')
hold on
plot(xL/dx,yL/dy,uL,vL,'w','LineWidth',2)
hold on
quiver(x(5:5:Nx),y(5:5:Ny),u_graph',v_graph','w','linewidth',2)
hold off
